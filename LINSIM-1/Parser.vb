﻿Imports System.Text.RegularExpressions
Imports LinSim.File

Namespace Parser
    Public Class ScriptReader
        Public Property Script As String()
        Public Property Host As Core.Host
        Public ReadOnly Property CurrentObject As String
        Public ReadOnly Property ObjectType As String
        Public ReadOnly Property CurrentLine As Integer = -1
        Sub New(fromHost As Core.Host, sourceFile As String)
            Host = fromHost
            Script = IO.File.ReadAllLines(sourceFile)
        End Sub
        Public ReadOnly Property EndOfScript As Boolean = False
        Private Function GetScript() As String
            _CurrentLine += 1
            If Script.Length <= CurrentLine Then
                _EndOfScript = True
                Return String.Empty
            End If
            Return Script(CurrentLine)
        End Function

        Dim target As Object

        Public Function Process() As ScriptResult
            Dim result As New ScriptResult
            Dim forceRead = False
            Dim lastLineResult As Boolean?
            While Not EndOfScript
                Dim text = GetScript().ToLower()
                If String.IsNullOrEmpty(text) AndAlso EndOfScript Then
                    GoTo last
                End If
                Dim data = Regex.Matches(text, "[\""].+?[\""]|[^ ]+") _
                    .Cast(Of Match)() _
                    .Select(Function(m) m.Value).ToList()

                If forceRead Then
                    If ObjectType = "file" Then
                        target = File.FileNode.GetFile(Host, CurrentObject)
                        forceRead = False
                    End If
                End If
                Select Case data(0)
                    Case "onerror"
                        If Not lastLineResult Then
                            result.ErrorMessage.Add(New ScriptMessage(MessageType.Error, data(1)))
                            result.Result = False
                        End If

                    Case "onsuccess"
                        If lastLineResult Then
                            result.ErrorMessage.Add(New ScriptMessage(MessageType.Success, data(1)))

                        End If
                    Case "read"
                        Select Case data(1)
                            Case "file"
                                _CurrentObject = data(2)
                                _ObjectType = "file"
                                forceRead = True
                        End Select

                    Case "check"
                        If String.IsNullOrEmpty(_CurrentObject) Then
                            result.ErrorMessage.Add(New ScriptMessage(MessageType.Error, "No file is read."))
                            GoTo last
                        End If

                        Select Case data(1)
                            Case "exist"
                                If ObjectType = "file" Then
                                    lastLineResult = target.IsExist()
                                End If
                            Case "permission"
                                If ObjectType = "file" Then
                                    Select Case data(2)
                                        Case "user"
                                            Select Case data(4)
                                                Case "readable"
                                                    lastLineResult = target.UserPermission.Readable
                                                Case "writable"
                                                    lastLineResult = target.UserPermission.Writable
                                                Case "executable"
                                                    lastLineResult = target.UserPermission.Executable
                                            End Select
                                        Case "group"
                                            Select Case data(4)
                                                Case "readable"
                                                    lastLineResult = target.GroupPermission.Readable
                                                Case "writable"
                                                    lastLineResult = target.GroupPermission.Writable
                                                Case "executable"
                                                    lastLineResult = target.GroupPermission.Executable
                                            End Select
                                        Case "other"
                                            Select Case data(4)
                                                Case "readable"
                                                    lastLineResult = target.OtherPermission.Readable
                                                Case "writable"
                                                    lastLineResult = target.OtherPermission.Writable
                                                Case "executable"
                                                    lastLineResult = target.OtherPermission.Executable
                                            End Select
                                    End Select

                                End If
                            Case "userowner"
                                If ObjectType = "file" Then
                                    If target.UserOwner = data(3) Then
                                        lastLineResult = True
                                    Else
                                        lastLineResult = False
                                    End If

                                End If
                        End Select
                End Select
            End While

last:
            Return result
        End Function

    End Class

    Public Class ScriptResult
        Public Property Result As Boolean = True
        Public Property ErrorMessage As New List(Of ScriptMessage)
    End Class

    Public Class ScriptMessage
        Public Property Type As MessageType
        Public Property Message As String

        Sub New(type As MessageType, message As String)
            Me.Type = type
            Me.Message = message
        End Sub
    End Class
    Public Enum MessageType
        Success
        [Error]
    End Enum
End Namespace