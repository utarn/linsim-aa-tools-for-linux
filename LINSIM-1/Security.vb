﻿Namespace Security

    Public Class FirewallZone
        Public Property Name As String
        Public Property IsActive As Boolean

        Public Shared Function GetFirewallZones(fromHost As Core.Host) As FirewallZone()
            Dim command = "firewall-cmd --get-zones"
            Dim allZones = fromHost.RunCommand(command).Replace(vbLf, "").Split(" ")
            command = "firewall-cmd --get-active-zone | head -n 1"
            Dim activeZone = fromHost.RunCommand(command).Replace(vbLf, "")

            Dim result(allZones.Length - 1) As FirewallZone
            For i = 0 To result.Length - 1
                result(i) = New FirewallZone()
                result(i).Name = allZones(i)
                result(i).IsActive = IIf(allZones(i) = activeZone, True, False)
            Next
            Return result
        End Function

    End Class

    Public Class FirewallRule
        Public Property InZone As String
        Public Property Interfaces As String()
        Public Property Sources As String()
        Public Property Services As String()
        Public Property Ports As String()
        Public Property IsMasquerade As Boolean
        Public Property ForwardPorts As String()
        Public Property ICMPBlock As String()
        Public Property RichRule As String()

        Public Shared Function GetFirewallRules(fromHost As Core.Host) As FirewallRule()
            Dim rules As New List(Of FirewallRule)
            For Each zone In FirewallZone.GetFirewallZones(fromHost)
                Dim command = $"firewall-cmd --permanent --zone={zone.Name} --list-all | tail -n +2 | head -n -1"
                Dim output = fromHost.RunCommand(command).Split(vbLf)
                Dim rule As New FirewallRule
                rule.InZone = zone.Name
                Try
                    rule.Interfaces = IIf(String.IsNullOrEmpty(output(0).Split(": ")(1).Trim()), Nothing, output(0).Split(": ")(1).Trim().Split(" "))
                Catch ex As Exception
                End Try
                Try
                    rule.Sources = IIf(String.IsNullOrEmpty(output(1).Split(": ")(1).Trim()), Nothing, output(1).Split(": ")(1).Trim().Split(" "))
                Catch ex As Exception
                End Try
                Try
                    rule.Services = IIf(String.IsNullOrEmpty(output(2).Split(": ")(1).Trim()), Nothing, output(2).Split(": ")(1).Trim().Split(" "))
                Catch ex As Exception
                End Try
                Try
                    rule.Ports = IIf(String.IsNullOrEmpty(output(3).Split(": ")(1).Trim()), Nothing, output(3).Split(": ")(1).Trim().Split(" "))
                Catch ex As Exception
                End Try
                Try
                    rule.IsMasquerade = IIf(output(4).Split(": ")(1) = "yes", True, False)
                Catch ex As Exception
                End Try
                Try
                    rule.ForwardPorts = IIf(String.IsNullOrEmpty(output(5).Split(": ")(1).Trim()), Nothing, output(5).Split(": ")(1).Trim().Split(" "))
                Catch ex As Exception
                End Try
                Try
                    rule.ICMPBlock = IIf(String.IsNullOrEmpty(output(6).Split(": ")(1).Trim()), Nothing, output(6).Split(": ")(1).Trim().Split(" "))
                Catch ex As Exception
                End Try
                Try
                    rule.RichRule = IIf(String.IsNullOrEmpty(output(7).Split(": ")(1).Trim()), Nothing, output(7).Split(": ")(1).Trim().Split(" "))
                Catch ex As Exception
                End Try

                rules.Add(rule)
            Next
            Return rules.ToArray()
        End Function

    End Class

End Namespace