﻿Imports System.Text.RegularExpressions

Namespace Service
    Public Class Package
        Public Property Name As String
        Public Property Version As String
        Public Property Source As String

        Public Shared Function GetInstalledPackages(fromHost As Core.Host) As List(Of Package)
            Network.Connection.AutoConnectInternet(fromHost)
            Dim command = "yum --noplugins list installed | sed  -E 's/[ ]{2,}/ /g'"
            Dim output = fromHost.RunCommand(command).Split(vbLf)
            Dim result As New List(Of Package)
            Dim startBit = False
            For Each line In output
                If line.StartsWith("Installed Packages") Then
                    startBit = True
                    Continue For
                End If
                Try
                    If startBit Then
                        Dim regex = New Regex("[ ]{2,}", RegexOptions.None)
                        Dim data = regex.Replace(line, " ").Split(" ")
                        Dim pack As New Package
                        pack.Name = data(0)
                        pack.Version = data(1)
                        pack.Source = data(2).Replace("@", "")
                        result.Add(pack)
                    End If
                Catch ex As Exception
                    Continue For
                End Try

            Next
            Return result
        End Function


    End Class
    Public Class Service
        Public Property Name As String
        Public Property IsLoaded As Boolean
        Public Property IsActive As Boolean
        Public Property State As String

        Public Shared Function GetServices(fromHost As Core.Host) As Service()
            Dim command = "systemctl list-units --type=service | tail -n +2 | head -n -7"
            Dim output = fromHost.RunCommand(command).Split(vbLf)
            Dim result(output.Length - 1) As Service
            For i = 0 To output.Length - 2
                result(i) = New Service
                Dim regex = New Regex("[ ]{2,}", RegexOptions.None)
                Dim data = regex.Replace(output(i).Replace("●", "").Trim(), " ").Split(" ")
                result(i).Name = data(0)
                result(i).IsLoaded = IIf(data(1) = "loaded", True, False)
                result(i).IsActive = IIf(data(2) = "active", True, False)
                result(i).State = data(3)
            Next
            Return result
        End Function

        Public Function IsEnabled(fromHost As Core.Host) As Boolean
            Dim command = $"systemctl is-enabled {Name}"
            Dim output = fromHost.RunCommand(command)
            If output.Contains("enabled") Then
                Return True
            Else
                Return False
            End If
        End Function
    End Class
    Public Class Target

    End Class

End Namespace