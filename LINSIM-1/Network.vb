﻿
Imports System.Text.RegularExpressions

Namespace Network
    Public Class Connection
        Public Property Name As String
        Public Property UUID As String
        Public Property Type As String
        Public Property Device As String

        Public Shared Sub AutoConnectInternet(fromHost As Core.Host)
            'Dim command = "curl -k -e ""https://login.rmutsb.ac.th/login.php"" ""https://login.rmutsb.ac.th/login.php"" -d ""_u=utharn.b&_p= rjtheking&a=a&web_host=login.rmutsb.ac.th&web_host4=auth4.rmutsb.ac.th&_ip4=&web_host6=auth6.rmutsb.ac.th&_ip6="""
            'fromHost.RunCommand(command)
        End Sub
        Shared Function GetAllConnections(fromHost As Core.Host) As Connection()
            Dim command = "nmcli c s | tail -n +2"
            Dim output = fromHost.RunCommand(command)
            Dim retrievedInfo = output.Split(vbLf)
            Dim result(retrievedInfo.Length - 2) As Connection
            For i = 0 To retrievedInfo.Length - 2
                Dim regex = New Regex("[ ]{2,}", RegexOptions.None)
                Dim data = regex.Replace(retrievedInfo(i), " ").Split(" ")
                result(i) = New Connection()
                result(i).Name = data(0)
                result(i).UUID = data(1)
                result(i).Type = data(2)
                result(i).Device = data(3)

            Next
            Return result
        End Function
    End Class

    Public Class ConnectionInfo
        Public Property Id As String
        Public Property UUID As String
        Public Property Type As String
        Public Property IsAutoConnect As Boolean
        Public Property Zone As String
        Public Property Method As String
        Public Property IPAddress As New List(Of String)
        Public Property Gateway As String
        Public Property DNS As New List(Of String)

        Public Shared Function GetConnectionInfoByUUID(fromHost As Core.Host, uuid As String) As ConnectionInfo
            Dim command = $"nmcli c s {uuid}"
            Return GetInfo(fromHost, command)
        End Function
        Public Shared Function GetConnectionInfoByName(fromHost As Core.Host, name As String) As ConnectionInfo
            Dim command = $"nmcli c s {name}"
            Return GetInfo(fromHost, command)
        End Function
        Public Shared Function GetConnectionInfoByDevice(fromHost As Core.Host, device As String) As ConnectionInfo
            Dim command = $"nmcli c s {device}"
            Return GetInfo(fromHost, command)
        End Function

        Public Shared Function GetInfo(fromHost As Core.Host, command As String) As ConnectionInfo
            Dim cInfo As New ConnectionInfo
            Dim output = fromHost.RunCommand(command)
            For Each line In output.Split(vbLf)
                If line.Contains("connection.id:") Then
                    cInfo.Id = line.Split(":")(1).Trim()
                End If
                If line.Contains("connection.uuid:") Then
                    cInfo.UUID = line.Split(":")(1).Trim()
                End If
                If line.Contains("connection.type:") Then
                    cInfo.Type = line.Split(":")(1).Trim()
                End If
                If line.Contains("connection.zone:") Then
                    cInfo.Zone = line.Split(":")(1).Trim()
                End If

                If line.Contains("connection.autoconnect:") Then
                    cInfo.IsAutoConnect = IIf(line.Split(":")(1).Trim() = "yes", True, False)
                End If

                If line.Contains("ipv4.method") Then
                    cInfo.Method = line.Split(":")(1).Trim()
                End If

                If line.Contains("IP4.ADDRESS") Then
                    cInfo.IPAddress.Add(line.Split(":")(1).Trim())
                End If
                If line.Contains("IP4.DNS") Then
                    cInfo.DNS.Add(line.Split(":")(1).Trim())
                End If

                If line.Contains("IP4.GATEWAY:") Then
                    cInfo.Gateway = line.Split(":")(1).Trim()
                End If

            Next

            Return cInfo
        End Function

    End Class

    Public Class Device
        Public Property Name As String
        Public Property Type As String
        Public Property State As String
        Public Property Connection As String

        Public Shared Function GetDevices(fromHost As Core.Host) As Device()
            Dim command = "nmcli d s | tail -n +2"
            Dim output = fromHost.RunCommand(command)
            Dim retrievedInfo = output.Split(vbLf)
            Dim result(retrievedInfo.Length - 2) As Device
            For i = 0 To retrievedInfo.Length - 2
                Dim regex = New Regex("[ ]{2,}", RegexOptions.None)
                Dim data = regex.Replace(retrievedInfo(i), " ").Split(" ")
                result(i) = New Device()
                result(i).Name = data(0)
                result(i).Type = data(1)
                result(i).State = data(2)
                result(i).Connection = data(3)
            Next
            Return result
        End Function
    End Class

    Public Class Route
        Public Property Type As String
        Public Property Via As String
        Public Property Device As String
        Public Property Metric As Integer

        Public Shared Function GetRoutes(fromHost As Core.Host) As Route
            Dim command = "ip route | grep default"
            Dim output = fromHost.RunCommand(command)
            Dim retrievedInfo = output.Split(vbLf)
            Dim result As New Route()
            For i = 0 To retrievedInfo.Length - 2
                Dim regex = New Regex("[ ]{2,}", RegexOptions.None)
                Dim data = regex.Replace(retrievedInfo(i), " ").Split(" ")
                result.Type = data(0)
                result.Via = data(2)
                result.Device = data(4)
                result.Metric = data(8)
            Next
            Return result
        End Function
    End Class

End Namespace
