﻿Namespace SELinux
    Public Class SEBoolean
        Public Property Name As String
        Public Property IsActive As Boolean

        Public Shared Function GetSEBooleans(fromHost As Core.Host) As SEBoolean()
            Dim command = "getsebool -a | sed 's/ --> /,/g'"
            Dim output = fromHost.RunCommand(command).Split(vbLf)
            Dim result(output.Length - 2) As SEBoolean
            For i = 0 To output.Length - 2
                result(i) = New SEBoolean()
                Dim data = output(i).Split(",")
                result(i).Name = data(0)
                result(i).IsActive = IIf(data(1) = "on", True, False)
            Next
            Return result
        End Function
    End Class

    Public Class SEPort
        Public Property Type As String
        Public Property Protocol As String
        Public Property Ports As New List(Of Integer)


        Public Shared Function GetSEPorts(fromHost As Core.Host) As SEPort()
            Dim command = "semanage port -ln | sed  -E 's/[ ]{2,}/ /g' | sed -E 's/, /,/g'"
            Dim output = fromHost.RunCommand(command).Split(vbLf)
            Dim result(output.Length - 2) As SEPort
            For i = 0 To output.Length - 2
                result(i) = New SEPort
                Dim data = output(i).Split(" ")
                result(i).Type = data(0)
                result(i).Protocol = data(1)
                For Each p In data(2).Split(",")
                    If p.Contains("-") Then
                        Dim startPort = Integer.Parse(p.Split("-")(0))
                        Dim endPort = Integer.Parse(p.Split("-")(1))
                        While startPort <= endPort
                            result(i).Ports.Add(startPort)
                            startPort += 1
                        End While
                    Else
                        result(i).Ports.Add(Integer.Parse(p))
                    End If
                Next
            Next
            Return result
        End Function
    End Class


End Namespace