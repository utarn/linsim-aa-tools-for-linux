﻿Imports System.Text.RegularExpressions

Namespace Disk
    Public Class MountPoint
        Public Property Source As String
        Public Property Target As String
        Public Property Type As String
        Public Property Options As String()

        Public Shared Function GetMountPoints(fromHost As Core.Host) As MountPoint()
            Dim command = "mount"
            Dim output = fromHost.RunCommand(command).Split(vbLf)
            Dim result(output.Length - 2) As MountPoint
            For i = 0 To output.Length - 2
                result(i) = New MountPoint()
                Dim data = output(i).Split(" ")
                result(i).Source = data(0)
                result(i).Target = data(2)
                result(i).Type = data(4)
                result(i).Options = data(5).Replace("(", "").Replace(")", "").Split(",")
            Next
            Return result
        End Function
    End Class
    Public Class DiskPartition
        Public Property Type As String
        Public Property Name As String
        Public Property FStype As String
        Public Property Mountpoint As String
        Public Property UUID As String
        Public Property Size As Long

        Public Shared Function GetDiskPartitions(fromHost As Core.Host) As DiskPartition()
            Dim command = " lsblk -nlb --output TYPE,NAME,FSTYPE,MOUNTPOINT,UUID,SIZE"
            Dim output = fromHost.RunCommand(command).Split(vbLf)
            Dim result(output.Length - 2) As DiskPartition
            For i = 0 To output.Length - 2
                result(i) = New DiskPartition()
                Dim regex = New Regex("[ ]{2,}", RegexOptions.None)
                Dim data = regex.Replace(output(i), " ").Split(" ")
                result(i).Type = data(0)
                Select Case result(i).Type
                    Case "disk", "rom"
                        result(i).Name = data(1)
                        result(i).Size = Long.Parse(data(2))
                    Case "part", "lvm"
                        result(i).Name = data(1)
                        result(i).FStype = data(2)
                        Select Case result(i).FStype
                            Case "LVM2_member"
                                result(i).UUID = data(3)
                                result(i).Size = Long.Parse(data(4))
                            Case Else
                                result(i).Mountpoint = data(3)
                                result(i).UUID = data(4)
                                result(i).Size = Long.Parse(data(5))
                        End Select
                End Select


            Next
            Return result
        End Function

    End Class

End Namespace