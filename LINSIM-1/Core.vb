﻿Imports System.Text
Imports System.Text.RegularExpressions
Imports LinSim.File
Imports Renci.SshNet
Namespace Core

    Public Class Host
        Public Property Hostname As String
        Public UserName As String
        Public Password As String

        Sub New(Optional hostname As String = "", Optional username As String = "", Optional password As String = "")
            Me.Hostname = hostname
            Me.UserName = username
            Me.Password = password
        End Sub

        Private _client As SshClient

        Private Function GetClient() As SshClient
            _client = New SshClient(Hostname, UserName, Password)
            _client.Connect()
            Return _client
        End Function

        Public Function RunCommand(command As String) As String
            Using client = GetClient()
                Dim ccc = client.CreateCommand(command).Execute()
                Return ccc
            End Using
        End Function


    End Class

End Namespace


