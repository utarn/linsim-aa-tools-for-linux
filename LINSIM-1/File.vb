﻿Namespace File
    Public Class FileNode
        Public Property Name As String
        Public Property FullName As String

        Public Property IsFile As Boolean
        Public Property IsDirectory As Boolean
        Public Property IsLink As Boolean
        Public Property IsExist As Boolean

        Public UserPermission As Permission
        Public GroupPermission As Permission
        Public OtherPermission As Permission
        Public Property Size As String
        Public Property UserOwner As String
        Public Property GroupOwner As String

        Public Property SEUserContext As String
        Public Property SEObjectContext As String
        Public Property SETypeContext As String
        Public Shared Function GetFile(fromHost As Core.Host, fullPath As String) As FileNode
            Dim f As New FileNode
            f.FullName = fullPath
            Dim commandText = $"if [ -f ""{f.FullName}"" ]; then echo ""true""; else echo ""false""; fi"
            Dim commandText2 = $"if [ -d ""{f.FullName}"" ]; then echo ""true""; else echo ""false""; fi"
            Dim commandText3 = $"if [ -l ""{f.FullName}"" ]; then echo ""true""; else echo ""false""; fi"
            f.IsFile = Boolean.Parse(fromHost.RunCommand(commandText))
            f.IsDirectory = Boolean.Parse(fromHost.RunCommand(commandText2))
            f.IsLink = Boolean.Parse(fromHost.RunCommand(commandText3))
            f.IsExist = f.IsFile OrElse f.IsDirectory OrElse f.IsLink
            If f.IsExist Then
                f.Name = f.FullName.Split("/").Last()
                Dim listCommand = $"ls -adl ""{f.FullName}"""
                Dim output = fromHost.RunCommand(listCommand)
                Dim data = output.Split(" ")
                f.UserPermission = New Permission(data(0).Substring(1, 3))
                f.GroupPermission = New Permission(data(0).Substring(4, 3))
                f.OtherPermission = New Permission(data(0).Substring(7, 3))
                f.UserOwner = data(2)
                f.GroupOwner = data(3)
                f.Size = data(4)
                Dim contextCommand = $"ls -Z ""{f.FullName}"""
                Dim output2 = fromHost.RunCommand(contextCommand)
                Dim data2 = output2.Split(" ")(3).Split(":")
                f.SEUserContext = data2(0)
                f.SEObjectContext = data2(1)
                f.SETypeContext = data2(2)
            End If
            Return f
        End Function
    End Class
    Public Class Permission
        Public Property Readable As Boolean
        Public Property Writable As Boolean
        Public Property Executable As Boolean

        Sub New(permissionText As String)
            For Each p In permissionText.ToCharArray()
                Select Case p
                    Case "r"
                        Readable = True
                    Case "w"
                        Writable = True
                    Case "x"
                        Executable = True
                End Select
            Next
        End Sub
    End Class
End Namespace
