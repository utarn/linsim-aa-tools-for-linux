﻿Namespace User
    Public Class UserAccount
        Public Property Username As String
        Public Property UID As Integer
        Public Property GID As Integer
        Public Property Comments As String
        Public Property HomeDirectory As String
        Public Property Shell As String

        Public Property Password As String
        Public Property LastChangedPasswordDate As Date
        Public Property WaitToChangePasswordDays As Integer
        Public Property NeedToChangePasswordDays As Integer
        Public Property WarnExpiringPasswordDays As Integer
        Public Property AccountDisableAfterPasswordExpireDay As Integer
        Public Property DisabledAccountDate As Date

        Public Shared Function GetUsers(fromHost As Core.Host) As UserAccount()
            Dim command = "cat /etc/passwd"
            Dim command2 = "cat /etc/shadow"
            Dim output = fromHost.RunCommand(command).Split(vbLf)
            Dim output2 = fromHost.RunCommand(command2).Split(vbLf)
            Dim users(output.Length - 2) As UserAccount
            For i = 0 To output.Length - 2
                Dim data = output(i).Split(":")
                users(i) = New UserAccount
                users(i).Username = data(0)
                users(i).UID = data(2)
                users(i).GID = data(3)
                users(i).Comments = data(4)
                users(i).HomeDirectory = data(5)
                users(i).Shell = data(6)

                Try
                    Dim shadowData = output2.First(Function(l) l.StartsWith(users(i).Username & ":")).Split(":")
                    users(i).Password = shadowData(1)
                    Try
                        users(i).LastChangedPasswordDate = New Date(1970, 1, 1).AddDays(Integer.Parse(shadowData(2)))

                    Catch ex As Exception

                    End Try
                    Try
                        users(i).WaitToChangePasswordDays = Integer.Parse(shadowData(3))
                    Catch ex As Exception

                    End Try
                    Try
                        users(i).NeedToChangePasswordDays = Integer.Parse(shadowData(4))
                    Catch ex As Exception

                    End Try
                    Try
                        users(i).WarnExpiringPasswordDays = Integer.Parse(shadowData(5))
                    Catch ex As Exception

                    End Try
                    Try
                        users(i).AccountDisableAfterPasswordExpireDay = Integer.Parse(shadowData(6))
                    Catch ex As Exception

                    End Try

                    Try
                        users(i).DisabledAccountDate = New Date(1970, 1, 1).AddDays(Integer.Parse(shadowData(7)))
                    Catch ex As Exception

                    End Try
                Catch ex As Exception

                End Try

            Next

            Return users

        End Function
        Public Shared Function GetUserByUsername(fromHost As Core.Host, username As String) As UserAccount
            Return GetUsers(fromHost).First(Function(u) u.Username = username)
        End Function
        Public Function IsInGroup(fromHost As Core.Host, groupName As String) As Boolean
            Dim command = $"id {Username}  | cut -f2 -d"" "" | cut -f2 -d="
            Dim output = fromHost.RunCommand(command)
            If output.Contains($"({groupName}") Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Function ListPrimaryGroup(fromHost As Core.Host) As String
            Dim command = $"id {Username}  | cut -f2 -d"" "" | cut -f2 -d="
            Dim output = fromHost.RunCommand(command)
            Return output
        End Function
        Public Function ListSecondaryGroup(fromHost As Core.Host) As String()
            Dim command = $"id {Username}  | cut -f3 -d"" "" | cut -f2 -d="
            Dim output = fromHost.RunCommand(command)
            Return output.Split(",")
        End Function
    End Class
    Public Class GroupAccount
        Public Property Name As String
        Public Property Id As Integer
        Public Property Members As New List(Of String)

        Public Shared Function GetGroups(fromHost As Core.Host) As GroupAccount()
            Dim command = "cat /etc/group"
            Dim output = fromHost.RunCommand(command).Split(vbLf)
            Dim groups(output.Length - 2) As GroupAccount
            For i = 0 To output.Length - 2
                Dim data = output(i).Split(":")
                groups(i) = New GroupAccount
                groups(i).Name = data(0)
                groups(i).Id = data(2)
                If Not String.IsNullOrEmpty(data(3)) Then
                    groups(i).Members = data(3).Split(",").ToList()
                End If

            Next

            Return groups

        End Function

        Public Overrides Function ToString() As String
            Return $"{Name} : {IIf(Members.Count = 0, "-", String.Join(",", Members))}"
        End Function
    End Class

End Namespace